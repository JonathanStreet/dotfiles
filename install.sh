#!/usr/bin/env bash

# Global settings {{{
set -e

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
TIMESTAMP="$(date +%Y-%m-%d_%H-%M)"

# Files & directories to ignore (regex)
IGNORELIST="""(\
backup_ \
.git \
README.md \
dependencies.sh \
install.sh\
)"""

# Replace spaces with | character, used for regex matching ignore list.
IGNORELIST="${IGNORELIST//[[:blank:]]/\|}"
# }}}

# Functions {{{
function dlFile {
    dir=$1  # Directory to place the file
    url=$2  # URL to download the file

    mkdir -p ${BASEDIR}/${dir}
    echo -n " * Downloading:    $url"
    $(cd ${BASEDIR}/${dir} && \
        curl -SJO "${url}" >/dev/null 2>&1)
    echo -e "\r * Downloaded:     "
    sleep 0.5
}

function backupFile {
    filename=$1  # Source file
    orig=$2 # Destination for symlink
    backupDir="${BASEDIR}/backup_${TIMESTAMP}"
    backupSubDir="$(dirname ${orig#${HOME}/})"
    backupFile="${backupDir}/${backupSubDir}/$(basename ${orig})"
    mkdir -p "${backupDir}/${backupSubDir}"
    cp ${orig} ${backupFile}
    echo " * Backed-up:     ${orig} => ${backupFile}"
}

function lnFile {
    src=$1  # Source file
    dest=$2 # Destination for symlink

    mkdir -p "$(dirname ${dest})"
    ln -sf ${src} ${dest}
    echo " * Symlinked:     ${src} => ${dest}"
    sleep 0.5
}
# }}}

# Main {{{
# Download an external files
printf "\nDownloading external files:\n===========================\n"
dlFile ".vim/colors" "https://raw.githubusercontent.com/morhetz/gruvbox/master/colors/gruvbox.vim"

# Symlink all dotfiles
printf "\nInstalling dotfiles:\n====================\n"
for file in $( find ${BASEDIR} -type f );
do

    # Skip files from the ignore list
    if [[ $file =~ ${IGNORELIST} ]]; then
        continue
    fi

    # Backup any existing files
    if [[ -f "${HOME}/${file/${BASEDIR}\//}" ]]; then
        backupFile ${file} "${HOME}/${file/${BASEDIR}\//}"
    fi

    # Symlink the new files
    lnFile ${file} "${HOME}/${file/${BASEDIR}\//}"

done

echo "Complete."
# }}}

# vim: fdm=marker
