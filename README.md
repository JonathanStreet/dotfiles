# Dotfiles

## Setup

Clone this repo:

    git clone https://bitbucket.org/JonathanStreet/dotfiles.git ~/dotfiles/

Run the installer:

    cd ~/dotfiles/ && chmod +x install.sh && ./install.sh


## Attributions

### Wallpapers

* [FiftyFootShadows.net (John Carey)](https://fiftyfootshadows.net)

### Vim

* [gruvbox](https://github.com/morhetz/gruvbox)

### Polybar

* [polywins](https://github.com/alnj/polywins)
* [polybar-spotify](https://github.com/PrayagS/polybar-spotify.git)

### Espanso

* [espanso docs](https://espanso.org/docs/)

