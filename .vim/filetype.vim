" Overrides for filetype
if has("autocmd")
    augroup filetypedetect
    "   Example:
    "   au BufNewFile,Bufread *.<extension> setf <filetype>
        au BufNewFile,Bufread *.csv setf csv
    augroup END
endif
