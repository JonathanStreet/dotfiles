" README
" =============================================================================
" To find further details about this .vimrc file, try the following -
"   :options
"   :help <option>
"
" This file is formatted with folding enabled -
"   :norm <Space> " Open/Close a fold
"   :norm zR/zM   " Open/Close all folds

" Basic Config {{{
set nocompatible                         " be IMproved, required
filetype off                             " required
filetype plugin indent on                " required
" }}}

" Preferences {{{
set t_Co=256                             " enable 256-colour support
set t_ut=                                " https://github.com/microsoft/WSL/issues/1706A
try
    colorscheme gruvbox                  " use 'gruvbox' theme
    let g:gruvbox_contrast_dark = 'hard' " use hard-contrast
    catch
        try
            colorscheme solarized        " use 'solarized' theme
            catch
        endtry
endtry
set background=dark                      " use dark background
set noerrorbells                         " prevent bell noises...
set vb t_vb=                             " ...please...
if exists('&belloff')
    set belloff=all                      " ...all of them
endif
set number                               " display line numbers
set numberwidth=4                        " set width of line numbers
set showmode                             " show current mode
set history=500                          " keep history
set tabstop=4                            " number of visual spaces per <TAB>
set softtabstop=4                        " number of spaces in tab when editing
set shiftwidth=4                         " number of spaces for indentation
set expandtab                            " tabs are spaces
set cursorline                           " enable position highlighting
set ruler                                " show the cursor position
set wrap                                 " enable word-wrapping
set textwidth=78                         " sets wrap-width
if has("mouse")
    set mouse=a                          " enable mouse support
endif
set wildmenu                             " visual autocomplete for command menu
set showmatch                            " highlight matching [{()}]
set incsearch                            " search as characters are entered
set hlsearch                             " highlight matches
syntax enable                            " enable syntax highlighting
set wildmenu                             " tab-completion menu
set path+=**                             " search all directories using :find
set wildignore+=**/node_modules/**       " folders to ignore
set hidden                               " stop the buffer from unloading
set modeline                             " Enable file modelines
let &colorcolumn="80,".join(range(120,999),",") " Higlight columns 80 & 120+
if has("autocmd")                        " Remain compatible
    augroup vimrc
        " Clear old autocmds for this group
        au!
        " No ruler on csv files
        au FileType csv set colorcolumn=
        " Have Vim jump to the last position when reopening a file
        au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
    augroup END
endif
" }}}

" Mappings {{{
" Normal mode
nnoremap <Space> za                      " Map space to toggle a fold
" Visual mode
vnoremap <Space> zf                      " Map space to create a fold
" }}}

" Performance {{{
set lazyredraw                           " redraw only when needed
set ttyfast                              " improve smoothness of redraws
" }}}

" StatusLine {{{
" https://stackoverflow.com/a/4588161
function! WordCount()
    let s:old_status = v:statusmsg
    let position = getpos(".")
    exe ":silent normal g\<c-g>"
    let stat = v:statusmsg
    let s:word_count = 0
    if stat != '--No lines in buffer--'
        let s:word_count = str2nr(split(v:statusmsg)[11])
        let v:statusmsg = s:old_status
    end
    call setpos('.', position)
    return s:word_count
endfunction

set statusline=                          " clear the statusline
set statusline+=%#CursorLine#            " set background
set statusline+=\ \ \ %t                 " tail of the filename
set statusline+=\ %y                     " filetype
set statusline+=\ %r                     " read-only flag
set statusline+=\ %m                     " modified flag
set statusline+=%=                       " left/right separator
set statusline+=%c,                      " cursor column
set statusline+=%l/%L                    " cursor line/total lines
set statusline+=\ wc:%{WordCount()}      " word count
set statusline+=\ %P                     " percentage through file
set statusline+=\ -\ 
    \%{strftime('
    \%Y-%m-%d\ -\ %H:%M\ \ \ 
    \')}                                 " date/time
set laststatus=2                         " display statusline
" }}}

" Includes {{{
if filereadable($HOME . "/.vimrc.local")
    source ~/.vimrc.local                " source .vimrc.local if available
endif
" }}}

" vim: foldmethod=marker
