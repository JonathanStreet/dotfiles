#!/usr/bin/env bash

## Install polybar {{{
# Note: Install polybar-git from AUR as 3.6.3 is broken w/ Manjaro-XCFCE
# pacman -Sy polybar
# }}}
## Install firefox {{{
pacman -Sy firefox
# }}}
## Install git {{{
pacman -Sy git
# }}}
## Install pandoc {{{
pacman -Sy pandoc
# }}}
## Install espanso {{{
espanso --version || espanso_installed=$?
if [[ ${espanso_installed} -ne 0 ]]; then

    # Create the $HOME/opt destination folder
    mkdir -p ~/opt

    # Download the AppImage inside it
    wget -O ~/opt/Espanso.AppImage 'https://github.com/federico-terzi/espanso/releases/download/v2.1.8/Espanso-X11.AppImage'

    # Make it executable
    chmod u+x ~/opt/Espanso.AppImage

    # Create the "espanso" command alias
    sudo ~/opt/Espanso.AppImage env-path register

    # Register espanso as a systemd service (required only once)
    espanso service check || exit_code=$?
    if [[ ${exit_code} -ne 0 ]]; then
        espanso service register
    fi

    # Start espanso
    espanso start

fi
## }}}

# vim: fdm=marker
